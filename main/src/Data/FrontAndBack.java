package Data;

public class FrontAndBack {
    private File file;
    private int direction;

    public FrontAndBack(File file, int direction) {
        this.file = file;
        this.direction = direction;//1表示前进（深入文件），2表示后退
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
