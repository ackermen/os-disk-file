package Data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import util.GetFileAbsolutePath;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class File implements Serializable {

    boolean isReadOnly; //是否只读
    boolean isSystemFile; //是否系统文件
    boolean isNormalFile; //是否普通文件
    boolean isFolder; //是否文件夹
    private String fileName;//文件名
    private int diskStartNum;//磁盘起始地址
    private File parent;//父目录
    private int length; //占用多少磁盘块
    private boolean isOpen;//记录文件是否已被打开；
    private int size;//文件大小
    private String absoluteName;
    private Date createTime;

    private String content;//文件内容;
    List<File> childrenFile;//子文件或子目录

    public File(boolean isReadOnly, boolean isSystemFile, boolean isNormalFile, boolean isFolder, String fileName, File parent) {
        this.isReadOnly = isReadOnly;
        this.isSystemFile = isSystemFile;
        this.isNormalFile = isNormalFile;
        this.isFolder = isFolder;
        this.fileName = fileName;
        this.parent = parent;
        this.content = "";
        this.createTime = new Date(System.currentTimeMillis());
        isOpen = false;
        if(isFolder == true){
            childrenFile = new ArrayList<>();
        }
        diskStartNum = 0;
        absoluteName = GetFileAbsolutePath.getAbsPath(this);
        length = 1;
        setAbsoluteNameProperty();
        setDiskNumProperty();
        setLengthProperty();
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
    }

    public boolean isSystemFile() {
        return isSystemFile;
    }

    public void setSystemFile(boolean systemFile) {
        isSystemFile = systemFile;
    }

    public boolean isNormalFile() {
        return isNormalFile;
    }

    public void setNormalFile(boolean normalFile) {
        isNormalFile = normalFile;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getDiskStartNum() {
        return diskStartNum;
    }

    public void setDiskStartNum(int diskStartNum) {
        this.diskStartNum = diskStartNum;
        setDiskNumProperty();
    }

    public File getParent() {
        return parent;
    }

    public void setParent(File parent) {
        this.parent = parent;
    }

    public int getLength() {
        length = (content.length())/64;
        if(content.length()%64 != 0){
            length += 1;
        }
        return length;
    }

    public void setLength(int length) {
        this.length = length;
        setLengthProperty();
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getSize() {
        return content.length();
    }

    public void setSize(int size) {
        this.size = size;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<File> getChildrenFile() {
        return childrenFile;
    }

    public void setChildrenFile(List<File> childrenFile) {
        this.childrenFile = childrenFile;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 描述：将日期转换为指定格式字符串
     * @param date  日期
     * @return sdf.format(date) 日期
     */
    public static String getDateStr(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
        return sdf.format(date);
    }

    public String getCreateTime() {
        return getDateStr(createTime);
    }

    public String getTypeName() {
        if(this.isFolder) {
            if (this.isSystemFile) return "系统文件夹";
            else return "普通文件夹";
        }else{
            if (this.isSystemFile) return "系统文件";
            else return "普通文件";
        }
    }

    /**
     * 是否存在子节点
     * @return 如果非空文件夹返回true
     */
    public boolean hasChild() {
        return !childrenFile.isEmpty();
    }
    /**
     * 是否非根目录
     * @return 根目录返回false
     */
    public boolean hasParent() {
        return parent != null;
    }

    public String getAbsoluteName() {
        return absoluteName;
    }

    public void setAbsoluteName(String absoluteName) {
        this.absoluteName = absoluteName;
        setAbsoluteNameProperty();
    }



    private transient StringProperty absoluteNameProperty = new SimpleStringProperty();
    private transient StringProperty diskNumProperty = new SimpleStringProperty();
    private transient StringProperty lengthProperty = new SimpleStringProperty();



    public void readObject()  {
        absoluteNameProperty = new SimpleStringProperty(absoluteName);
        diskNumProperty = new SimpleStringProperty(String.valueOf(diskStartNum));
        lengthProperty = new SimpleStringProperty(String.valueOf(length));
    }

    public String getAbsoluteNameProperty() {
        return absoluteNameProperty.get();
    }

    public String getDiskNumProperty() {
        return diskNumProperty.get();
    }

    public String getLengthProperty() {
        return lengthProperty.get();
    }
    public void setAbsoluteNameProperty() {
        this.absoluteNameProperty.set(this.absoluteName);
    }

    public void setDiskNumProperty() {
        this.diskNumProperty.set(String.valueOf(diskStartNum));
    }

    public void setLengthProperty() {
        this.lengthProperty.set(String.valueOf(length));
    }



    public StringProperty absoluteNamePropertyProperty() {
        return absoluteNameProperty;
    }
    public StringProperty diskNumPropertyProperty() {
        return diskNumProperty;
    }
    public StringProperty lengthPropertyProperty() {
        return lengthProperty;
    }

    public int getNeed(){
        int frontLength = length;
        return getLength() - frontLength;
    }


}
