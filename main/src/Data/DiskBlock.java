package Data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import util.GetFileAbsolutePath;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class DiskBlock implements Serializable {
    private int number; //磁盘号
    private int nextDiskNum; //用于记录文件的下一个磁盘块 （若为0，表示该磁盘块空闲，-1表示文件内容结束，254表示磁盘损坏，不能使用）
    private String type; //存储的类型(文件，文件夹，系统文件，空);
    private File file; //指向文件或文件夹

    public DiskBlock(int number, int nextDiskNum, String type, File file) {
        this.number = number;
        this.nextDiskNum = nextDiskNum;
        this.type = type;
        this.file = file;

        setNumberProperty();
        setNextDiskNumProperty();
        setTypeProperty();
        setFileProperty();
    }



    public transient StringProperty numberProperty = new SimpleStringProperty();
    public transient StringProperty nextDiskNumProperty = new SimpleStringProperty();
    public transient StringProperty typeProperty = new SimpleStringProperty();
    public transient StringProperty fileProperty = new SimpleStringProperty();

    public String getNumberProperty() {
        return numberProperty.get();
    }

    public StringProperty numberPropertyProperty() {
        return numberProperty;
    }

    public String getNextDiskNumProperty() {
        return nextDiskNumProperty.get();
    }

    public StringProperty nextDiskNumPropertyProperty() {
        return nextDiskNumProperty;
    }

    public String getTypeProperty() {
        return typeProperty.get();
    }

    public StringProperty typePropertyProperty() {
        return typeProperty;
    }

    public String getFileProperty() {
        return fileProperty.get();
    }

    public StringProperty filePropertyProperty() {
        return fileProperty;
    }

    public void allowNextBlock(){
        setNextDiskNum(nextDiskNum);
        setType(type);
        setObject(file);
    }

    public void clearBlock() {
        setNextDiskNum(0);
        setType("");
        setObject(null);
    }
    public void setObject(File file){
        this.file = file;
        if (file != null){
            this.fileProperty.bind(file.absoluteNamePropertyProperty());
        } else {
            this.fileProperty.unbind();
            setFileProperty();
        }
    }

    private void setNumberProperty(){
        numberProperty.set(String.valueOf(number));
    }
    private void setNextDiskNumProperty() {
        nextDiskNumProperty.set(String.valueOf(nextDiskNum));
    }
    private void setTypeProperty() {
        this.typeProperty.set(type);
    }

    public void  setFileProperty(){
        if(file!=null) {
            this.fileProperty.set(GetFileAbsolutePath.getAbsPath(file));
        }else {
            this.fileProperty.set("");
        }
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNextDiskNum() {
        return nextDiskNum;
    }

    public void setNextDiskNum(int nextDiskNum) {
        this.nextDiskNum = nextDiskNum;
        this.setNextDiskNumProperty();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        setTypeProperty();
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        numberProperty = new SimpleStringProperty(String.valueOf(number));
        nextDiskNumProperty = new SimpleStringProperty(String.valueOf(nextDiskNum));
        typeProperty = new SimpleStringProperty(type);
        fileProperty = new SimpleStringProperty(file == null ? "" : file.getAbsoluteName());
        setObject(file);
    }
}
