package Data;

import java.io.Serializable;

public class StoreDiskBlock implements Serializable {
    private int number; //磁盘号
    private int nextDiskNum; //用于记录文件的下一个磁盘块 （若为0，表示该磁盘块空闲，-1表示文件内容结束，254表示磁盘损坏，不能使用）
    private String type; //存储的类型(文件，文件夹，系统文件，空);

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNextDiskNum() {
        return nextDiskNum;
    }

    public void setNextDiskNum(int nextDiskNum) {
        this.nextDiskNum = nextDiskNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
