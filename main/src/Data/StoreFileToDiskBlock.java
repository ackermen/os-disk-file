package Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static controller.MainController.fat;


public class StoreFileToDiskBlock implements Serializable {

    StoreDiskBlock[] storeDiskBlocks;


    public StoreFileToDiskBlock() {
        storeDiskBlocks = new StoreDiskBlock[128];
        for(int i = 0; i < 128; i++){
            storeDiskBlocks[i] = new StoreDiskBlock();
        }
    }

    public StoreDiskBlock[] getStoreDiskBlocks() {
        return storeDiskBlocks;
    }

    public void setStoreDiskBlocks(StoreDiskBlock[] storeDiskBlocks) {
        this.storeDiskBlocks = storeDiskBlocks;
    }



    public void convert(File f,DiskBlock[] blocks){
        List<File> childrenFile =  f.getChildrenFile();
        int t ;
        if(f.getParent() != null){
            t = f.getDiskStartNum();

          do{
              f.setOpen(false);
              f.readObject();
              blocks[t].setType(storeDiskBlocks[t].getType());
              blocks[t].setNextDiskNum(storeDiskBlocks[t].getNextDiskNum());
              blocks[t].setObject(f);
             /* blocks[t].setFileProperty();*/
              t = storeDiskBlocks[t].getNextDiskNum();
          }while (t != -1);
          f.setAbsoluteNameProperty();
          f.setLengthProperty();
          f.setDiskNumProperty();
        }
        if(childrenFile != null) {
            for (File file1 : childrenFile) {
                t = file1.getDiskStartNum();
                do{
                    file1.setOpen(false);
                    file1.readObject();
                    blocks[t].setType(storeDiskBlocks[t].getType());
                    blocks[t].setNextDiskNum(storeDiskBlocks[t].getNextDiskNum());
                    blocks[t].setObject(file1);
                  /*  blocks[t].setFileProperty();*/
                    t = storeDiskBlocks[t].getNextDiskNum();
                }while (t != -1);
                if(file1.isFolder){
                    convert(file1,blocks);
                }
            }
        }
    }

}

