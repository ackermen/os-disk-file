package Main;

import Data.DiskBlock;
import Data.StoreDiskBlock;
import Data.StoreFileToDiskBlock;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.Vector;

import static controller.MainController.fat;

public class Main extends Application {

    static final int PATH_WIDTH = 1410;
    static final int PATH_HEIGHT = 900;
    public static Vector<Stage> fileAppAdditionStageList = new Vector();

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL resource = getClass().getClassLoader().getResource("Main-view.fxml");
        if(resource == null){
            throw new RuntimeException("Main-view.fxml找不到");
        }
        fxmlLoader.setLocation(resource);
        Scene scene = new Scene(fxmlLoader.load(), PATH_WIDTH, PATH_HEIGHT);
        stage.setTitle("模拟磁盘文件系统");
        stage.getIcons().add(new Image(getClass().getClassLoader().getResource("icon/操作系统.png").toExternalForm()));
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String[] args) {
        launch();
        StoreFileToDiskBlock s = new StoreFileToDiskBlock();
        DiskBlock[] t = fat.getDiskBlocks();
        StoreDiskBlock[] d = s.getStoreDiskBlocks();
        for(int i = 0; i < 128; i++){
            d[i].setType(t[i].getType());
            d[i].setNumber(t[i].getNumber());
            d[i].setNextDiskNum(t[i].getNextDiskNum());
        }
        try {
            ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream(new File("DiskBlock.sfs")));
            oo.writeObject(d);
            oo.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}