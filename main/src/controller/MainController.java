package controller;

import Data.*;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

import javafx.stage.Stage;
import javafx.util.Duration;


import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import operations.createFileOperations;
import operations.deleteOperations;
import operations.openOperations;
import util.*;

import static com.sun.org.apache.xml.internal.security.keys.keyresolver.KeyResolver.iterator;
import static controller.RenameController.sentParam;
import static operations.renameOperations.renameFile;
import static util.FileRW.readDisk;
import static util.FileRW.writeDisk;
import static util.GetFileAbsolutePath.getAbsPath;
import static util.UpdatePieChart.updatePieChart;

public class MainController {


    @FXML
    private ImageView backBtn;

    @FXML
    private Label data;

    @FXML
    private TableColumn<DiskBlock, String> diskAddress;

    @FXML
    public  PieChart diskChart;

    @FXML
    private TableColumn<DiskBlock, String> diskNext;

    @FXML
    private TableColumn<DiskBlock, String> diskPath;

    @FXML
    private TableColumn<DiskBlock, String> diskType;

    @FXML
    private TableView<DiskBlock> diskView;

    @FXML
    private FlowPane fileExplorer;

    @FXML
    private AnchorPane filePreview;

    @FXML
    private AnchorPane fileExplorerPane;

    @FXML
    private TreeView<String> fileView;

    @FXML
    private ImageView frontBtn;

    @FXML
    private ImageView homeBtn;

    @FXML
    private Label hours;

    @FXML
    private ImageView iconMes;

    @FXML
    private Label memoryMes;

    @FXML
    private Label nameLabel;

    @FXML
    private Label nameMes;

    @FXML
    private TextField pathText;

    @FXML
    private Label pathLabel;

    @FXML
    private Label rwLabel;

    @FXML
    private Label rwMes;

    @FXML
    private Label sizeLabel;

    @FXML
    private ImageView searchBtn;

    @FXML
    private Label timeMes;

    @FXML
    private Label typeMes;

    @FXML
    private ImageView upBtn;

    // 右侧磁盘块
    private ObservableList<DiskBlock> data_Block;

    public static FAT fat;

    // 创建文件
    private MenuItem createFileItem;
    // 创建文件夹
    private MenuItem createFolderItem;
    // 打开文件
    private MenuItem openItem;
    // 重命名文件
    private MenuItem renameItem;
    // 删除文件
    private MenuItem deleteItem;
    // 显示文件属性
    // private MenuItem propItem;
    // 更改文件属性
    private MenuItem changItem;
    public static File rootFile;
    public static TreeItem<String> rootNode;
    public static File currentFile;
    public static TreeItem<String> recentNode;
    public static Map<File, TreeItem<String>> treeMap = new HashMap<>();
    public static Stack<FrontAndBack> frontStack = new Stack<>() ;
    public static Stack<FrontAndBack> backStack = new Stack<>();
    public static Map<File,Label> icons = new HashMap<>();
    Label selectIcon = null;

    private ContextMenu createContextMenu;
    private ContextMenu actionContextMenu;

    @FXML
    void initialize() throws IOException, ClassNotFoundException {
        //项目初始化
        timeInit();
        fat = new FAT();
        contextMenuInit();
        menuItemAction();
        readDisk();
        rootNode = new TreeItem<String>("C:",new ImageView("icon/disk.png"));
        fileView.setRoot(rootNode);
        rootNode.setExpanded(true);
        treeMap.put(rootFile,rootNode);
        currentFile = rootFile;
        recentNode = rootNode;
        fileExplorer.setAlignment(Pos.TOP_LEFT);
        fileExplorer.setHgap(15);
        fileExplorer.setVgap(20);
        fileExplorer.setPadding(new Insets(0,10,0,10));
        openOperations.openFolder(fileExplorer, currentFile, icons);
        TreeViewIterator treeViewIterator = new TreeViewIterator();
        treeViewIterator.iterator(fileView);
        ArrayList<File> tmpFiles = new FileIterator().iterator(rootFile);
        for (File file : tmpFiles) {
            if (file.isFolder()) {
                LabelMouseEventForFolder(icons.get(file));
            } else {
                LabelMouseEventForFile(icons.get(file));
            }
        }
        initTable();

        writeDisk();
        pathText.setText("C:\\");
        TreeViewIterator.tmpFileExplorer = fileExplorer;
        TreeViewIterator.tmpPathText = pathText;
        updatePieChart(diskChart);
        FileController.setDiskChart(diskChart);

    }

    void initTable(){
        data_Block = FXCollections.observableArrayList(fat.getDiskBlocks());

        diskAddress.setCellValueFactory(new PropertyValueFactory<>("numberProperty"));
        diskAddress.setSortable(false);
        diskAddress.setResizable(true);

        diskNext.setCellValueFactory(new PropertyValueFactory<>("nextDiskNumProperty"));
        diskNext.setSortable(false);
        diskNext.setResizable(true);

        diskType.setCellValueFactory(new PropertyValueFactory<>("typeProperty"));
        diskType.setSortable(false);
        diskType.setResizable(true);

        diskPath.setCellValueFactory(new PropertyValueFactory<>("fileProperty"));
        diskPath.setSortable(false);
        diskPath.setResizable(true);

        diskView.setItems(data_Block);
        diskView.setEditable(false);
        diskView.setEditable(false);
    }

    //更新磁盘空间图像

    private void contextMenuInit() {
        createFileItem = new MenuItem("新建文件");
        createFolderItem = new MenuItem("新建文件夹");

        openItem = new MenuItem("打开");
        deleteItem = new MenuItem("删除");
        renameItem = new MenuItem("重命名");
        changItem = new MenuItem("切换文件读写属性");

        createContextMenu = new ContextMenu(createFileItem, createFolderItem);
        actionContextMenu = new ContextMenu(openItem, deleteItem, renameItem,  changItem);
    }

    public void timeInit() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(1), event -> updateTime())
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    public void updateTime() {
        Date date = new Date();
        String hour = String.format("%tH", date);
        String minute = String.format("%tM", date);
        String second = String.format("%tS", date);
        String year = String.format("%ty", date);
        String month = String.format("%tm", date);
        String day = String.format("%td", date);
        this.hours.setText(hour + ":" + minute + ":" + second);
        this.data.setText("20" + year + "/" + month + "/" + day);
    }
// 预览文件鼠标悬停移入事件
    public void setFilePreview(File newFile) {
        iconMes.setVisible(true);
        Image icon;
        nameMes.setText("文件名称：" + newFile.getFileName());
        if (newFile.getTypeName().equals("普通文件") || newFile.getTypeName().equals("系统文件")) {
            typeMes.setText("文件类型：" + newFile.getTypeName());
            icon = new Image("icon/文本文件.png");
        } else {
            typeMes.setText("文件类型：" + newFile.getTypeName());
            icon = new Image("icon/文件夹.png");
        }
        if(newFile.getTypeName().equals("普通文件") || newFile.getTypeName().equals("系统文件")){
            if (newFile.isReadOnly()) {
                rwMes.setText("文件属性：只读");
            } else rwMes.setText("文件属性：读写");
        }
        iconMes.setImage(icon);
        memoryMes.setText("占用空间：" + newFile.getSize() + "B");
        timeMes.setText("建立时间：" + newFile.getCreateTime());
    }
// 预览文件鼠标悬停移除事件
    public void removeFilePreview(File newFile){
            nameMes.setText("");
            typeMes.setText("");
            rwMes.setText("");
            iconMes.setVisible(false);
            memoryMes.setText("");
            timeMes.setText("");
    }
// 点击选中文件信息事件
    public void selectAction(File file) {
            nameLabel.setText("文件名: " + file.getFileName());
            sizeLabel.setText("文件大小:" + file.getSize() + "B");
            if (file.isReadOnly()) {
                rwLabel.setText("文件属性:只读");
            } else rwLabel.setText("文件属性:读写");
            pathLabel.setText("绝对路径:" + GetFileAbsolutePath.getAbsPath(file));
    }

    //FlowPane的鼠标点击事件
    @FXML
    public void clickFileExplorer(MouseEvent mouseEvent) {
        fileExplorer.addEventHandler(MouseEvent.MOUSE_CLICKED,(MouseEvent click) -> {
            if(click.getButton() == MouseButton.SECONDARY && !actionContextMenu.isShowing()){  //右键点击事件
                createContextMenu.show(fileExplorer,click.getScreenX(),click.getScreenY());
            }else {
                createContextMenu.hide();
            }
        });
    }
//新建文件
    public void menuItemAction(){
        createFileItem.setOnAction(ActionEvent -> {
            int sum = 0;
            for(int i = 2; i < 128; i++){
                if(fat.getDiskBlocks()[i].getNextDiskNum() == 0){
                    sum++;
                }
            }
            if(sum == 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("磁盘空间不足");
                alert.show();
                return;
            }
            File file = createFileOperations.createFile(fileExplorer,icons,actionContextMenu);
            TreeViewIterator treeViewIterator = new TreeViewIterator();
            treeViewIterator.Clear();
            treeViewIterator.iterator(fileView);
            LabelMouseEventForFile(icons.get(file));
            fat.applySpace(file);
            try {
                writeDisk();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
            updatePieChart(diskChart);
        });

        createFolderItem.setOnAction(ActionEvent -> {
            int sum = 0;
            for(int i = 2; i < 128; i++){
                if(fat.getDiskBlocks()[i].getNextDiskNum() == 0){
                    sum++;
                }
            }
            if(sum == 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("磁盘空间不足");
                alert.show();
                return;
            }
            File folder = createFileOperations.createFolder(fileExplorer,icons,actionContextMenu);
            TreeViewIterator treeViewIterator = new TreeViewIterator();
            treeViewIterator.Clear();
            treeViewIterator.iterator(fileView);
            LabelMouseEventForFolder(icons.get(folder));
            fat.applySpace(folder);
            try {
                writeDisk();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
            updatePieChart(diskChart);
        });
    }

    //返回上一级
    @FXML
    void upperFolder(MouseEvent event) {
        if (currentFile != rootFile) {
            if(currentFile.isFolder()){
                backStack.push(new FrontAndBack(currentFile,1));
                recentNode = treeMap.get(currentFile);
                recentNode.setExpanded(false);
                String pathName = getAbsPath(currentFile.getParent());
                pathText.setText(pathName);
            }
            currentFile = currentFile.getParent();
            openOperations.openFolder(fileExplorer, currentFile, icons);
        }
    }

     void LabelMouseEventForFile(Label icon){
        icon.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                ((Label) event.getSource()).setStyle("-fx-background-color: rgba(116,194,238,0.35);");
                setFilePreview(Objects.requireNonNull(FindLabelToFile.findFile(icons, icon)));

            }
        });

        icon.setOnMouseExited(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                ((Label) event.getSource()).setStyle("-fx-background-color: transparent;");
                removeFilePreview(FindLabelToFile.findFile(icons,icon));
            }
        });

        icon.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Label src = (Label) event.getSource();
                if (event.getButton() == MouseButton.SECONDARY && event.getClickCount() == 1) {
                    //右键打开文件
                    actionContextMenu.getItems().get(0).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        if(currentFile != null) {

                            if (currentFile.isFolder()) {
                                openOperations.openFolder(fileExplorer, currentFile, icons);
                            } else {
                                if(currentFile.isOpen()){
                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                    alert.setContentText("文件已打开!");
                                    alert.show();
                                    return;
                                }
                                try {
                                    currentFile.setOpen(true);
                                    openOperations.openFile();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }
                    });
                    //右键删除文件
                    actionContextMenu.getItems().get(1).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        File deleteFile = currentFile;
                        if(!deleteFile.isOpen()) {
                            currentFile = currentFile.getParent();
                            if (currentFile != rootFile && currentFile.isFolder()) {
                                openOperations.openFolder(fileExplorer, currentFile, icons);
                            }
                            deleteOperations.deleteFile(fileExplorer, icons, deleteFile, fat);
                            updatePieChart(diskChart);
                            try {
                                writeDisk();
                            } catch (IOException | ClassNotFoundException e) {
                                throw new RuntimeException(e);
                            }
                        }else {
                            System.out.println("1");
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setContentText("文件未关闭!");
                            alert.show();
                        }
                    });
                    //右键重命名文件
                    actionContextMenu.getItems().get(2).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        try {
                            sentParam(fileView,pathText);
                            renameFile();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    });
                    //右键切换读写属性
                    actionContextMenu.getItems().get(3).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        if (currentFile.isReadOnly()) {
                            currentFile.setReadOnly(false);
                        } else {
                            currentFile.setReadOnly(true);
                        }
                        try {
                            writeDisk();
                        } catch (IOException | ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                        currentFile = currentFile.getParent();
                    });
                    actionContextMenu.show(src, event.getScreenX(), event.getScreenY());
                } else if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                    System.out.println("打开文件");
                    currentFile = FindLabelToFile.findFile(icons, src);
                    if(currentFile != null) {
                        if (currentFile.isFolder()) {
                            openOperations.openFolder(fileExplorer, currentFile, icons);
                            String pathName = getAbsPath(currentFile);
                            pathText.setText(pathName);
                        } else {
                            if(currentFile.isOpen()){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.setContentText("文件已打开!");
                                alert.show();
                                return;
                            }
                            try {
                                currentFile.setOpen(true);
                                openOperations.openFile();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                } else if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                    System.out.println("选择");
                    File file = FindLabelToFile.findFile(icons, icon);
                    selectAction(file);
                } else {
                    actionContextMenu.hide();
                }
            }
        });
    }

    void LabelMouseEventForFolder(Label icon){
        icon.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((Label) event.getSource()).setStyle("-fx-background-color: rgba(116,194,238,0.35);");
                setFilePreview(Objects.requireNonNull(FindLabelToFile.findFile(icons, icon)));
            }
        });

        icon.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((Label) event.getSource()).setStyle("-fx-background-color: transparent;");
                removeFilePreview(FindLabelToFile.findFile(icons,icon));
            }
        });

        icon.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Label src = (Label) event.getSource();
                System.out.println(src);
                if (event.getButton() == MouseButton.SECONDARY && event.getClickCount() == 1) {
                    //右键打开文件
                    actionContextMenu.getItems().get(0).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        backStack.push(new FrontAndBack(currentFile,2));
                        if(currentFile != null) {
                            if (currentFile.isFolder()) {
                                openOperations.openFolder(fileExplorer, currentFile, icons);
                                recentNode = treeMap.get(currentFile);
                                rootNode.setExpanded(true);
                                recentNode.setExpanded(true);
                                String pathName = getAbsPath(currentFile);
                                pathText.setText(pathName);
                            } else {
                                try {
                                    openOperations.openFile();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }
                    });
                    //右键删除文件
                    actionContextMenu.getItems().get(1).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        File deleteFile = currentFile;
                        if(deleteFile.getChildrenFile().size() != 0){
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setContentText("文件夹不为空");
                            alert.show();
                            return;
                        }
                        currentFile = currentFile.getParent();
                        if (currentFile != rootFile && currentFile.isFolder()) {
                            openOperations.openFolder(fileExplorer, currentFile, icons);
                        }
                        deleteOperations.deleteFile(fileExplorer, icons, deleteFile,fat);
                        updatePieChart(diskChart);
                        TreeViewIterator treeViewIterator = new TreeViewIterator();
                        treeViewIterator.Clear();
                        treeViewIterator.iterator(fileView);
                        recentNode = treeMap.get(currentFile);
                        rootNode.setExpanded(true);
                        recentNode.setExpanded(true);
                        try {
                            writeDisk();
                        } catch (IOException | ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                    });
                    //右键重命名文件
                    actionContextMenu.getItems().get(2).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        try {
                            sentParam(fileView,pathText);
                            renameFile();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    });
                    //右键切换读写属性
                    actionContextMenu.getItems().get(3).setOnAction(ActionEvent -> {
                        currentFile = FindLabelToFile.findFile(icons, src);
                        if (currentFile.isReadOnly()) {
                            currentFile.setReadOnly(false);
                        } else {
                            currentFile.setReadOnly(true);
                        }
                        try {
                            writeDisk();
                        } catch (IOException | ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                        currentFile = currentFile.getParent();
                    });
                    actionContextMenu.show(src, event.getScreenX(), event.getScreenY());
                } else if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                    System.out.println("打开文件");
                    currentFile = FindLabelToFile.findFile(icons, icon);
                    backStack.push(new FrontAndBack(currentFile,2));
                    System.out.println(currentFile.getFileName());
                    if (currentFile.isFolder()) {
                        openOperations.openFolder(fileExplorer, currentFile, icons);
                        recentNode = treeMap.get(currentFile);
                        rootNode.setExpanded(true);
                        recentNode.setExpanded(true);
                        String pathName = getAbsPath(currentFile);
                        pathText.setText(pathName);
                    } else {
                        if(currentFile.isOpen()){
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setContentText("文件已打开!");
                            alert.show();
                            return;
                        }
                        try {
                            openOperations.openFile();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }

                } else if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                    System.out.println("选择");
                    File file = FindLabelToFile.findFile(icons, icon);
                    selectAction(file);
                } else {
                    actionContextMenu.hide();
                }
            }
        });
    }

    @FXML
    void front(MouseEvent event) {
        if(event.getButton()== MouseButton.PRIMARY) {
            if(frontStack.size() != 0) {
                FrontAndBack frontAndBack = frontStack.pop();
                File file1 = frontAndBack.getFile();
                if(frontAndBack.getDirection() == 1){
                    currentFile = file1;
                    if(icons.get(file1)!= null){
                        backStack.push(new FrontAndBack(file1,2));
                        openOperations.openFolder(fileExplorer,currentFile,icons);
                        recentNode = treeMap.get(currentFile);
                        recentNode.setExpanded(false);
                        String pathName = getAbsPath(currentFile.getParent());
                        pathText.setText(pathName);
                    }
                }
                if(frontAndBack.getDirection() == 2){
                    if(file1 != rootFile){
                        if(icons.get(file1) != null){
                            currentFile = file1.getParent();
                            backStack.push(new FrontAndBack(file1,1));
                            openOperations.openFolder(fileExplorer,currentFile,icons);                        recentNode = treeMap.get(currentFile);
                            recentNode.setExpanded(false);
                            String pathName = getAbsPath(currentFile.getParent());
                            pathText.setText(pathName);

                        }
                    }
                }
            }
        }
    }

    @FXML
    void home(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY){
            backStack.clear();
            frontStack.clear();
            currentFile = rootFile;
            openOperations.openFolder(fileExplorer,currentFile,icons);
            recentNode = treeMap.get(currentFile);
            recentNode.setExpanded(false);
            String pathName = getAbsPath(currentFile.getParent());
            pathText.setText(pathName);
        }
    }

    @FXML
    void back(MouseEvent event) {
        if(event.getButton()== MouseButton.PRIMARY) {
            if(backStack.size() != 0) {
                FrontAndBack frontAndBack = backStack.pop();
                File file1 = frontAndBack.getFile();
                if(frontAndBack.getDirection() == 1){
                    currentFile = file1;
                    if(icons.get(file1)!= null){
                        frontStack.push(new FrontAndBack(file1,2));
                        openOperations.openFolder(fileExplorer,currentFile,icons);
                        recentNode = treeMap.get(currentFile);
                        recentNode.setExpanded(false);
                        String pathName = getAbsPath(currentFile.getParent());
                        pathText.setText(pathName);
                    }
                }
                if(frontAndBack.getDirection() == 2) {
                   if(file1 != rootFile){
                       if(icons.get(file1) != null){
                           currentFile = file1.getParent();
                           frontStack.push(new FrontAndBack(file1,1));
                           openOperations.openFolder(fileExplorer,currentFile,icons);
                           recentNode = treeMap.get(currentFile);
                           recentNode.setExpanded(false);
                           String pathName = getAbsPath(currentFile.getParent());
                           pathText.setText(pathName);
                       }
                   }
                }
            }
        }
    }

    @FXML
    void Search(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY){
            String fileName = pathText.getText();
            File file = null;
            for(int i = 2; i < 128 ; i++){
                if(fat.getDiskBlocks()[i].getFile() != null) {
                    if (fat.getDiskBlocks()[i].getFile().getAbsoluteName().equals(fileName)) {
                        file = fat.getDiskBlocks()[i].getFile();
                    }
                }
            }
            if(file == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("没找到该文件");
                alert.show();
            }else {
                backStack.push(new FrontAndBack(currentFile,2));
                currentFile = file;
                if (currentFile.isFolder()) {
                    openOperations.openFolder(fileExplorer, currentFile, icons);
                    recentNode = treeMap.get(currentFile);
                    rootNode.setExpanded(true);
                    recentNode.setExpanded(true);
                } else {
                    if(currentFile.isOpen()){
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("文件已打开!");
                        alert.show();
                        return;
                    }
                    try {
                        currentFile.setOpen(true);
                        openOperations.openFile();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

}
