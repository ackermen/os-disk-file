package operations;

import Data.File;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class renameOperations {
    public static void renameFile() throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL url = renameOperations.class.getClassLoader().getResource("Rename-view.fxml");
        if (url == null) {
            throw new RuntimeException("Rename-view.fxml Not Found.");
        }
        fxmlLoader.setLocation(url);
        Scene scene = new Scene(fxmlLoader.load());
        Stage stage = new Stage();
        stage.setTitle("重命名");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    public static boolean isAbleToRename(File file, String renameStr) {
        for (File file1 : file.getParent().getChildrenFile()) {
            if (file1 == file) {
                continue;
            }
            if (file1.getFileName().equals(renameStr)) {
                return false;
            }
        }
        return true;
    }
}
