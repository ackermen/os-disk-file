package operations;

import Data.File;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import util.RandomStr;


import java.util.Map;
import java.util.Vector;

import static controller.MainController.currentFile;

public class createFileOperations {
    public static File createFile(FlowPane flowPane, Map<File, Label> icons, ContextMenu actionContextMenu){
        String rdStr = RandomStr.getRandomString(5);
        File file = new File(false,false,true,false,"新建文件"+rdStr,currentFile);
        System.out.println("创建文件");
        Label icon = new Label(file.getFileName(),new ImageView("icon/文本文件.png"));
        icon.setContentDisplay(ContentDisplay.TOP);
        icon.setWrapText(false);
        icon.setPrefWidth(100);
        icon.setAlignment(Pos.CENTER);
        icons.put(file,icon);
        Vector<File> folders = new Vector<>();
        Vector<File> files = new Vector<>();

        for(File file1 : currentFile.getChildrenFile()){
            if(file1.isFolder()){
                folders.add(file1);
                flowPane.getChildren().remove(icons.get(file1));
            }else {
                files.add(file1);
               flowPane.getChildren().remove(icons.get(file1));
            }
        }

        for(File folder1 : folders){
            flowPane.getChildren().add(icons.get(folder1));
        }
        for(File file1 : files){
            flowPane.getChildren().add(icons.get(file1));
        }
        flowPane.getChildren().add(icon);

        currentFile.getChildrenFile().add(file);

        return file;
    }

    public static File createFolder(FlowPane flowPane, /*File currentFile,*/ Map<File, Label> icons, ContextMenu actionContextMenu){
        String rdStr = RandomStr.getRandomString(5);
        File file = new File(false,false,true,true,"新建文件夹"+rdStr,currentFile);
        System.out.println("创建文件夹");
        Label icon = new Label(file.getFileName(),new ImageView("icon/文件夹.png"));
        icon.setContentDisplay(ContentDisplay.TOP);
        icon.setWrapText(false);
        icon.setPrefWidth(100);
        icon.setAlignment(Pos.CENTER);
        icons.put(file, icon);
        Vector<File> folders = new Vector<>();
        Vector<File> files = new Vector<>();
        for(File file1 : currentFile.getChildrenFile()){
            if(file1.isFolder()){
                folders.add(file1);
                flowPane.getChildren().remove(icons.get(file1));
            }else {
                files.add(file1);
                flowPane.getChildren().remove(icons.get(file1));
            }
        }
        for(File folder1 : folders){
            if(folder1 != null) {
                flowPane.getChildren().add(icons.get(folder1));
            }
        }
        flowPane.getChildren().add(icon);
        for(File file1 : files){
            if(file1 !=null) {
                flowPane.getChildren().add(icons.get(file1));
            }
        }
        currentFile.getChildrenFile().add(file);
        return file;
    }
}
