package operations;

import Data.FAT;
import Data.File;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import util.FileIterator;

import java.util.ArrayList;
import java.util.Map;

public class deleteOperations {
    public static void deleteFile(FlowPane fileExplorer, Map<File, Label> icons, File needToDelete, FAT fat) {
        fileExplorer.getChildren().remove(icons.get(needToDelete));
        if (needToDelete.isFolder()) {
            ArrayList<File> deleteFiles = new ArrayList<>();
            deleteFiles = new FileIterator().iterator(needToDelete);
            for (File file : deleteFiles) {
                icons.remove(file);
                fat.reclaimSpace(file);
            }
        }
        icons.remove(needToDelete);
        needToDelete.getParent().getChildrenFile().remove(needToDelete);
        fat.reclaimSpace(needToDelete);
    }
}
