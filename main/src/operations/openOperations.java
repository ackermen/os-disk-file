package operations;

import Data.File;
import controller.FileController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Vector;

import static controller.MainController.currentFile;
import static controller.MainController.rootFile;

public class openOperations {
    public static void openFolder(FlowPane fileExplorer, File currentFile, Map<File, Label> icons) {
        fileExplorer.getChildren().clear();
        if (!currentFile.getChildrenFile().isEmpty()) {
            Vector<File> folders = new Vector<>();
            Vector<File> files = new Vector<>();

            for(File file1 : currentFile.getChildrenFile()){
                if(file1.isFolder()){
                    folders.add(file1);
                }else {
                    files.add(file1);
                }
            }

            for (File folder : folders) {
                fileExplorer.getChildren().add(icons.get(folder));
            }
            for (File file : files) {
                fileExplorer.getChildren().add(icons.get(file));
            }
            System.out.println("opened");
        }
    }
    public static void openFile() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL url = renameOperations.class.getClassLoader().getResource("File-view.fxml");
        if (url == null) {
            throw new RuntimeException("File-view.fxml Not Found.");
        }
        fxmlLoader.setLocation(url);
        Scene scene = new Scene(fxmlLoader.load());
        Stage stage = new Stage();
        stage.setTitle("文本编辑器");
        stage.setResizable(false);
        stage.setScene(scene);
        currentFile.setOpen(true);
        File file = currentFile;
        stage.setOnCloseRequest(event -> {
            file.setOpen(false);
            if (currentFile.getParent() == rootFile) {
                currentFile = rootFile;
            } else {
                currentFile = currentFile.getParent();
            }
        });
        stage.show();
    }
}
