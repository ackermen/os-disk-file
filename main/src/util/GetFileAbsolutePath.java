package util;

import Data.File;

import java.util.Stack;

import static controller.MainController.rootFile;

public class GetFileAbsolutePath {
    public static String getAbsPath(File file) {
        if (file != null) {
            Stack<String> filePath = new Stack<>();
            if (file != rootFile) {
                File tmpFile = file;
                while (tmpFile != rootFile) {
                    filePath.push(tmpFile.getFileName());
                    tmpFile = tmpFile.getParent();
                }
            }
            String fileAbsPath = "C:\\";
            while (!filePath.isEmpty()) {
                if (filePath.size() == 1) {
                    fileAbsPath = fileAbsPath + filePath.peek();
                } else {
                    fileAbsPath = fileAbsPath + filePath.peek() + "\\";
                }
                filePath.pop();
            }
            return fileAbsPath;
        } else return null;
    }
}