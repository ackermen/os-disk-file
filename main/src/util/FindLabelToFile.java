package util;

import Data.File;
import javafx.scene.control.Label;

import java.util.Map;

public class FindLabelToFile {
    public static File findFile(Map<File, Label> map, Label label) {
        for (File file : map.keySet()) {
            if (map.get(file) == label){
                return file;
            }
        }
        return null;
    }
}
