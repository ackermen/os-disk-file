package util;

import Data.DiskBlock;
import javafx.scene.chart.PieChart;



import static controller.MainController.fat;

public class UpdatePieChart {
   public static void updatePieChart(PieChart diskChart){
       DiskBlock[] fatData = fat.getDiskBlocks();
       int cnt = 0;
       for(int i = 0; i < 128; ++i){
           if(fatData[i].getNextDiskNum() == 0)
               cnt++;
       }
       // 清除旧数据
       diskChart.getData().clear();
       double a= (double) cnt /128;
       double b=(1-a);
       // 添加新数据
       diskChart.getData().addAll(new PieChart.Data("剩余空间\n"+a*100+"%", 100 * a), new PieChart.Data("已用空间\n"+b*100+"%", 100*b));
   }
}
