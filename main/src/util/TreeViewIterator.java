package util;

import Data.File;
import Data.FrontAndBack;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import operations.openOperations;

import java.util.Map;
import java.util.Objects;

import static controller.MainController.*;
import static util.GetFileAbsolutePath.getAbsPath;

public class TreeViewIterator {

    public static FlowPane tmpFileExplorer;
    public static TextField tmpPathText;

    void addSearch(File file, TreeItem<String> recentNode) {
        if (file.isFolder()) {
            for (File file1 : file.getChildrenFile()) {
                if (file1.isFolder()) {
                    TreeItem<String> tmpNode = new TreeItem<>(file1.getFileName(),new ImageView("icon/folder.png"));
                    recentNode.getChildren().add(tmpNode);
                    treeMap.put(file1, tmpNode);
                    tmpNode.setExpanded(true);
                    addSearch(file1, tmpNode);
                }
            }
        }
    }

    public void iterator(TreeView<String> fileView) {
        fileView.setRoot(rootNode);
        rootNode.setExpanded(true);
        fileView.setCellFactory((TreeView<String> p) -> new TextFieldTreeCellImpl());
        addSearch(rootFile, rootNode);
    }

    public void Clear() {
        rootNode.getChildren().clear();
    }

    public final class TextFieldTreeCellImpl extends TreeCell<String> {
        private TextField textField;

        public TextFieldTreeCellImpl() {
            this.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                        if (getTreeItem() != null) {
                            String pathName = null;
                            //File:Key     TreeItem:Value
                            for (Map.Entry<File, TreeItem<String>> entry : treeMap.entrySet()) {
                                if (getTreeItem() == entry.getValue()) {
                                    pathName = getAbsPath(entry.getKey());
                                    System.out.println(getAbsPath(entry.getKey()));
                                    break;
                                }
                            }
                            backStack.push(new FrontAndBack(currentFile,1));
                            tmpFileExplorer.getChildren().removeAll(tmpFileExplorer.getChildren());
                            recentNode = getTreeItem();
                            currentFile = Objects.requireNonNull(FindTreeItemToFile.findFile(treeMap, recentNode));
                            openOperations.openFolder(tmpFileExplorer, currentFile, icons);
                            tmpPathText.setText(pathName);
                            System.out.println(pathName);
                        }
                    }
                    if (event.getButton() == MouseButton.SECONDARY && event.getClickCount() == 1) {
                        currentFile = Objects.requireNonNull(FindTreeItemToFile.findFile(treeMap, recentNode));
                        openOperations.openFolder(tmpFileExplorer, currentFile, icons);
                    }
                }
            });
        }
            @Override
            public void startEdit() {
                super.startEdit();

                if (textField == null) {
                    createTextField();
                }
                setText(null);
                setGraphic(textField);
                textField.selectAll();
            }

            @Override
            public void cancelEdit() {
                super.cancelEdit();
                setText((String) getItem());
                setGraphic(getTreeItem().getGraphic());
            }

            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    if (isEditing()) {
                        if (textField != null) {
                            textField.setText(getString());
                        }
                        setText(null);
                        setGraphic(textField);
                    } else {
                        setText(getString());
                        setGraphic(getTreeItem().getGraphic());
                    }
                }
            }

            private void createTextField() {
                textField = new TextField(getString());
                textField.setOnKeyReleased((KeyEvent t) -> {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                });

            }

            private String getString() {
                return getItem() == null ? "" : getItem().toString();
            }
    }

    public static void sentExplorerParam(FlowPane fileExplorer){
        tmpFileExplorer = fileExplorer;
    }

}

