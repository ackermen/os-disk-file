package util;

import Data.File;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static controller.MainController.icons;


public class FileConvertToIcon {
    //递归
    void deepConvert(File file) {
        ArrayList<File> folders = new ArrayList<>();
        for (File file1 : file.getChildrenFile()) {
            Label icon;
            if(file1.isFolder()) {
                folders.add(file1);
                icon = new Label(file1.getFileName(),new ImageView("icon/文件夹.png"));
            } else {
                icon = new Label(file1.getFileName(),new ImageView("icon/文本文件.png"));
            }
            icon.setContentDisplay(ContentDisplay.TOP);
            icon.setWrapText(false);
            icon.setPrefWidth(100);
            icon.setAlignment(Pos.CENTER);
            icons.put(file1,icon);
        }
        for (File file1 : folders) {
            deepConvert(file1);
        }
    }

    public void convert(File rootFile) {
        deepConvert(rootFile);
    }
}
