package util;

import Data.File;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;

import java.util.Map;

public class FindTreeItemToFile {
    public static File findFile(Map<File, TreeItem<String>> map, TreeItem<String> treeItem) {
        for (File file : map.keySet()) {
            if (map.get(file) == treeItem){
                return file;
            }
        }
        return null;
    }
}
