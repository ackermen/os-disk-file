package util;

import Data.File;

import java.util.ArrayList;

public class FileIterator {

    ArrayList<File> files;

    void search(File file) {
        if (file.isFolder()) {
            ArrayList<File> folders = new ArrayList<>();
            for (File file1 : file.getChildrenFile()) {
                files.add(file1);
                if (file1.isFolder()) {
                    folders.add(file1);
                }
            }
            for (File folder1 : folders) {
                search(folder1);
            }
        }
    }

    public ArrayList<File> iterator(File file) {
        files = new ArrayList<>();
        search(file);
        return files;
    }
}
