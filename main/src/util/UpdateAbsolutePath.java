package util;

import Data.File;

import java.util.List;

public class UpdateAbsolutePath {
    public static void updateChildrenPath(File file){
        List<File> Children = file.getChildrenFile();
        file.setAbsoluteName(GetFileAbsolutePath.getAbsPath(file));
        if(Children != null) {
            for (File file1 : Children) {
                if (file1.isFolder()) {
                    file1.setAbsoluteName(GetFileAbsolutePath.getAbsPath(file1));
                    updateChildrenPath(file1);
                } else {
                    file1.setAbsoluteName(GetFileAbsolutePath.getAbsPath(file1));
                }
            }
        }
    }
}
